## Move Datastore data to Big Query using Dataflow
![](/dataflow.png)

### How to run
1. you must have installed jdk 8 or above and maven
2. clone the code
3. execute the following line updating the value args with corresponding project

```java
mvn compile exec:java \
-Dexec.mainClass=io.dataflow.DatastoreToBigQuery \
-Dexec.args=" \
  <Datastore table name> \ 
  <BigQuery dataset> \
  <BigQuery dataset table> \
  <Target gcp projectId> \
  --runner=DataflowRunner \
  --project=<gcp project id> \
  --region=<gcp project region> \
  --tempLocation=<google storage bucket>/temp \
  --stagingLocation=<google storage bucket>/staging \
  --serviceAccount=dataflow-datastore-bigquery@ach-tin-dev.iam.gserviceaccount.com \
  --templateLocation=<google storage bucket>/templates/datastoreToBigQueryJob"
```

Example:
```java
mvn compile exec:java \
-Dexec.mainClass=io.dataflow.DatastoreToBigQuery \
-Dexec.args=" \
  action_export \
  datastore_to_bigquery \
  action \
  minka-ach-dw \
  --runner=DataflowRunner \
  --project=ach-tin-dev \
  --region=us-central1 \
  --tempLocation=gs://datastore-to-bigquery-job/temp \
  --stagingLocation=gs://datastore-to-bigquery-job/staging \
  --serviceAccount=dataflow-datastore-bigquery@ach-tin-dev.iam.gserviceaccount.com \
  --templateLocation=gs://datastore-to-bigquery-job/templates/datastoreToBigQueryJob"
```