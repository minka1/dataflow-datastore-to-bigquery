package io.dataflow;

import io.dataflow.TableRowCoder;
import io.dataflow.CloudSQLUtils;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.jdbc.JdbcIO;
import org.apache.beam.sdk.io.jdbc.JdbcIO.RowMapper;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;


import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import com.google.api.services.bigquery.model.TableRow;

import org.apache.beam.sdk.io.gcp.bigquery.TableRowJsonCoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CloudSQLToBigQuery {
    
    private static final Logger logger = LoggerFactory.getLogger(DatastoreToBigQuery.class);

    public static void main(String[] args) {
        
        if (args.length != 15) {
            throw new Error("Missing args");
        }
        System.out.print(args);
        // Gcp
        String projectId = "";
        
        // CloudSQL
        String cloudSQLUser = args[0];
        String cloudSQLPassword = args[1];
        String cloudSQLDatabase = args[2];
        String cloudSQLTable = args[3];
        String cloudSQLInstance = args[4];
        
        // BigQuery
        String bigQueryDataset = args[5];
        String bigQueryTable = args[6];
        String targetProjectId = args[7];

        for (String arg : args) {
            logger.debug("main:arg:{}", arg);

            if (arg.contains("--project=")) {
                projectId = arg.replace("--project=", "");
            }
        }
        
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass("com.mysql.jdbc.Driver");
        dataSource.setJdbcUrl(
                "jdbc:mysql://google/" + 
                  cloudSQLDatabase +
                  "?cloudSqlInstance=" + cloudSQLInstance + 
                  "&socketFactory=com.google.cloud.sql.mysql.SocketFactory"
        );
        dataSource.setUser(cloudSQLUser);
        dataSource.setPassword(cloudSQLPassword);
        dataSource.setMaxPoolSize(10);
        dataSource.setInitialPoolSize(6);

        JdbcIO.DataSourceConfiguration config = JdbcIO.DataSourceConfiguration.create(dataSource);
        
        String[] pipelineArgs = new String[]{args[8], args[9], args[10], args[11], args[12], args[13], args[14]};
        PipelineOptions pipelineOptions = PipelineOptionsFactory.fromArgs(pipelineArgs).create();
        Pipeline pipeline = Pipeline.create(pipelineOptions);
        
       
        pipeline
            .apply(
                "Read from Cloud SQL",
                JdbcIO.<HashMap<String, Object>>read()
                    .withDataSourceConfiguration(config)
                    .withCoder(TableRowCoder.of())
                    .withRowMapper(new RowMapper<HashMap<String, Object>>() {
                        @Override
                        public HashMap<String, Object> mapRow(ResultSet resultSet) throws Exception {
                          return TableRowMapper.asMap(resultSet, cloudSQLTable);
                        }
                      })
                    .withOutputParallelization(false)
                    .withQuery("SELECT "
                      + "transferId, "
                      + "txId, "
                      + "source, "
                      + "sourceWallet, "
                      + "sourceSigner, "
                      + "sourceBank, "
                      + "target, "
                      + "targetWallet, "
                      + "targetSigner, "
                      + "targetBank, "
                      + "amount, "
                      + "symbol, "
                      + "type, "
                      + "status, "
                      + "description, "
                      + "created, "
                      + "updated, "
                      + "labels->'$.transactionPurpose' AS purpose, "
                      + "labels->'$.sourceChannel' AS 'sourceChannel', "
                      + "labels->'$.tx_id' AS txLabelsId, "
                      + "error->'$.code' AS errorCode, "
                      + "error->'$.message' AS 'errorMessage' "
                      + "FROM transfer")
            )
            .apply(
                "HashMap to TableRow",
                ParDo.of(new HashMapToTableRowFn())
            )
            .setCoder(TableRowJsonCoder.of())
            .apply("Write to BigQuery",
                BigQueryIO.writeTableRows()
                    .to(BigQueryUtils.getTableReferenceOfBigQuery(targetProjectId, bigQueryDataset, bigQueryTable)) // Passed as an argument from the command line
                    .withSchema(CloudSQLUtils.schema()) // The schema for the BigQuery table
                    .ignoreUnknownValues() // Ignore any values passed but not defined on the table schema
                    .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED) // Create the BigQuery table if it doesn't exist
                    .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE) // Rewrite to the BigQuery table.
              );

        pipeline.run();
    }
    
    private static class HashMapToTableRowFn extends DoFn<HashMap<String, Object>, TableRow> {

        @ProcessElement
        public void processElement(final @Element HashMap<String, Object> map, final OutputReceiver<TableRow> receiver) {
            TableRow tableRow = new TableRow();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
              tableRow.set(entry.getKey(), entry.getValue());
            }

            receiver.output(tableRow);
        }
    }
}
