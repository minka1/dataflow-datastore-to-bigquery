package io.dataflow;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableSchema;
import io.dataflow.TableSchemaBuilder;
import java.util.ArrayList;
import java.util.List;

public class CloudSQLUtils {

    public static TableSchema schema() {
        return TableSchemaBuilder.create()
            .stringField("transferId")
            .stringField("txId")
            .stringField("source")
            .stringField("sourceWallet")
            .stringField("sourceSigner")
            .stringField("sourceBank")
            .stringField("target")
            .stringField("targetWallet")
            .stringField("targetSigner")
            .stringField("targetBank")
            .floatField("amount")
            .stringField("symbol")
            .stringField("type")
            .stringField("status")
            .stringField("description")
            .stringField("created")
            .stringField("updated")
            .stringField("purpose")
            .stringField("sourceChannel")
            .stringField("txLabelsId")
            .stringField("errorCode")
            .stringField("errorMessage")
            .schema();
    }

}
