package io.dataflow;

import org.apache.beam.sdk.io.gcp.datastore.DatastoreIO;
import com.google.datastore.v1.Query;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import com.google.api.services.bigquery.model.TableRow;
import org.apache.beam.sdk.transforms.Filter;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.values.TypeDescriptor;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.dataflow.DatastoreUtils;
import io.dataflow.BigQueryUtils;

import static org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED;
import static org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE;

public class DatastoreToBigQuery {

    private static final Logger logger = LoggerFactory.getLogger(DatastoreToBigQuery.class);

    public static void main(String[] args) {
        if (args.length != 11) {
            throw new Error("Missing args");
        }
        System.out.print(args);
        // Gcp
        String projectId = "";
        // Datastore
        String entity = args[0];
        // Big Query
        String dataset = args[1];
        String table = args[2];
        String targetProjectId = args[3];

        for (String arg : args) {
            logger.debug("main:arg:{}", arg);

            if (arg.contains("--project=")) {
                projectId = arg.replace("--project=", "");
            }
        }

        String[] pipelineArgs = new String[]{args[4], args[5], args[6], args[7], args[8], args[9], args[10]};
        PipelineOptions pipelineOptions = PipelineOptionsFactory.fromArgs(pipelineArgs).create();
        Pipeline pipeline = Pipeline.create(pipelineOptions);

        Query.Builder queryBuilder = Query.newBuilder();
        queryBuilder.addKindBuilder()
          .setName(entity)
          .build();

        pipeline
          .apply(
            "Read from Datastore",
            DatastoreIO
              .v1()
              .read()
              .withProjectId(projectId)
              .withQuery(queryBuilder.build())
          )
          .apply("To TableRows Raw",
            MapElements
              .into(TypeDescriptor.of(TableRow.class))
              .via(DatastoreUtils::toTableRow)
          )
          .apply(
            "Filter TableRows",
            Filter.by(Objects::nonNull)
          )
          .apply(
            "Write to BigQuery",
            BigQueryIO
              .<TableRow>writeTableRows()
              .withJsonSchema(BigQueryUtils.getTableSchemaOfBigQuery())
              .to(BigQueryUtils.getTableReferenceOfBigQuery(targetProjectId, dataset, table))
              .withCreateDisposition(CREATE_IF_NEEDED)
              .withWriteDisposition(WRITE_TRUNCATE)
          );
        pipeline.run();
    }
}
