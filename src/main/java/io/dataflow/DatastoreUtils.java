package io.dataflow;

import com.google.api.services.bigquery.model.TableRow;
import com.google.datastore.v1.Entity;
import com.google.datastore.v1.Value;
import java.util.Map;

public class DatastoreUtils {

    public static String getValue(Entity entity, String[] keys) {
        Map<String, Value> props = entity.getPropertiesMap();
        String value = "";
        for (int i = 0; i < keys.length; i++) {
            if (props.get(keys[i]) != null) {
                if (!props.get(keys[i]).hasEntityValue()) {
                    if (keys[i] == "code") {
                        value = String.valueOf(props.get(keys[i]).getIntegerValue());
                    } else {
                        value = props.get(keys[i]).getStringValue();
                    }
                } else {
                    props = props.get(keys[i]).getEntityValue().getPropertiesMap();
                }
            } else {
                return null;
            }
        }
        return value;
    }

    public static TableRow toTableRow(Entity entity) {

        return new TableRow()
          .set("action_id", getValue(entity, new String[]{"action_id"}))
          .set("amount", getValue(entity, new String[]{"amount"}))
          .set("source", getValue(entity, new String[]{"source"}))
          .set("target", getValue(entity, new String[]{"target"}))
          .set("symbol", getValue(entity, new String[]{"symbol"}))
          .set("error", new TableRow()
            .set("code", getValue(entity, new String[]{"error", "code"}))
            .set("message", getValue(entity, new String[]{"error", "message"}))
          )
          .set("labels", new TableRow()
            .set("tx_ref", getValue(entity, new String[]{"labels", "tx_ref"}))
            .set("tx_id", getValue(entity, new String[]{"labels", "tx_id"}))
            .set("type", getValue(entity, new String[]{"labels", "type"}))
            .set("status", getValue(entity, new String[]{"labels", "status"}))
            .set("description", getValue(entity, new String[]{"labels", "description"}))
            .set("created", getValue(entity, new String[]{"labels", "created"}))
            .set("updated", getValue(entity, new String[]{"labels", "updated"}))
            .set("hash", getValue(entity, new String[]{"labels", "hash"}))
          )
          .set("snapshot", new TableRow()
            .set("source", new TableRow()
              .set("wallet", new TableRow()
                .set("handle", getValue(entity, new String[]{"snapshot", "source", "wallet", "handle"}))
              )
              .set("signer", new TableRow()
                .set("handle", getValue(entity, new String[]{"snapshot", "source", "signer", "handle"}))
                .set("labels", new TableRow()
                  .set("bankName", getValue(entity, new String[]{"snapshot", "source", "signer", "labels", "bankName"}))
                  .set("bankAccountType", getValue(entity, new String[]{"snapshot", "source", "signer", "labels", "bankAccountType"}))
                  .set("bankAccountNumber", getValue(entity, new String[]{"snapshot", "source", "signer", "labels", "bankAccountNumber"}))
                  .set("routerReference", getValue(entity, new String[]{"snapshot", "source", "signer", "labels", "routerReference"}))
                )
              )
            )
            .set("target", new TableRow()
              .set("wallet", new TableRow()
                .set("handle", getValue(entity, new String[]{"snapshot", "target", "wallet", "handle"}))
              )
              .set("signer", new TableRow()
                .set("handle", getValue(entity, new String[]{"snapshot", "target", "signer", "handle"}))
                .set("labels", new TableRow()
                  .set("bankName", getValue(entity, new String[]{"snapshot", "target", "signer", "labels", "bankName"}))
                  .set("bankAccountType", getValue(entity, new String[]{"snapshot", "target", "signer", "labels", "bankAccountType"}))
                  .set("bankAccountNumber", getValue(entity, new String[]{"snapshot", "target", "signer", "labels", "bankAccountNumber"}))
                  .set("routerReference", getValue(entity, new String[]{"snapshot", "target", "signer", "labels", "routerReference"}))
                )
              )
            )
          );
    }
}
