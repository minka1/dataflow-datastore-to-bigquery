package io.dataflow;

import com.google.api.services.bigquery.model.TableReference;
import com.google.common.io.Resources;
import java.io.IOException;
import java.nio.charset.Charset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BigQueryUtils {

    private static final Logger logger = LoggerFactory.getLogger(BigQueryUtils.class);

    public static String getTableSchemaOfBigQuery() {
        try {
            return Resources
              .toString(BigQueryUtils.class
                .getClassLoader()
                .getResource("bigQuerySchema.json"), Charset.forName("utf-8")
              );
        } catch (IOException ex) {
            logger.error("getTableSchemaJson:catch:{}", ex.getMessage());
            return "";
        }
    }

    public static TableReference getTableReferenceOfBigQuery(
      String projectId,
      String datasetId,
      String tableId
    ) {
        return new TableReference()
          .setProjectId(projectId)
          .setDatasetId(datasetId)
          .setTableId(tableId);
    }
}
